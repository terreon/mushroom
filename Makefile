all: jshint uglify-js docs dist

node_modules/%:
	npm install $*

uglify-js: ./node_modules/uglify-js
	./node_modules/.bin/uglifyjs \
		< js/mushroom.js \
		> js/mushroom.min.js

jshint: ./node_modules/jshint
	./node_modules/.bin/jshint \
		--config jshint-config.js \
		--verbose \
		--show-non-errors \
		js/mushroom.js

.PHONY: docs
docs:
	cd docs && $(MAKE) html

sdist:
	python setup.py sdist

bdist_wheel:
	python setup.py bdist_wheel --universal

dist: sdist bdist_wheel

upload:
	python setup.py bdist_wheel --universal upload

release: dist upload

clean:
	rm -rf build mushroom.egg-info docs/_build dist

clean-node_modules:
	rm -rf node_modules

clean-all: clean clean-node_modules
