test('Exceptions', function() {
	function throw_exception() {
		throw new mushroom.Exception();
	}
	try {
		throw_exception();
	} catch (ex) {
		ok(ex instanceof Error, 'mushroom.Exception is not an instance of Error');
		ok(ex instanceof mushroom.Exception, 'mushroom.Exception instanceof mushroom.Exception');
		ok(ex.name === 'mushroom.Exception', 'mushroom.Exception.name === "mushroom.Exception"');
		ok(new Error().name === 'Error', 'Error.name === "Error"');
	}
});

test('IllegalState', function() {
	function throw_exception() {
		throw new mushroom.IllegalState("Time is up!");
	}
	try {
		throw_exception();
	} catch (ex) {
		ok(ex instanceof Error, 'mushroom.IllegalState instanceof Error');
		ok(ex instanceof mushroom.Exception, 'mushroom.IllegalState instanceof mushroom.Exception');
		ok(ex instanceof mushroom.IllegalState, 'mushroom.IllegalState instanceof mushroom.IllegalState');
		ok(ex.name === 'mushroom.IllegalState', 'mushroom.IllegalState.name === "mushroom.IllegalState"');
		ok(new Error().name === 'Error', 'Error.name === "Error"');
	}
});


test('Signal.send without handlers', function() {
	var signal = new mushroom.Signal();
	signal.send();
	expect(0);
});

test('Signal.send with one argument', function() {
	var signal = new mushroom.Signal();
	var dataReceived;
	signal.connect(function(data) {
		dataReceived = data;
	});
	signal.send(42);
	strictEqual(dataReceived, 42, 'dataReceived === 42');
});

test('Signal.send with one argument', function() {
	var signal = new mushroom.Signal();
	var aReceived;
	var bReceived;
	signal.connect(function(a, b) {
		aReceived = a;
		bReceived = b;
	});
	signal.send(1, 2);
	strictEqual(aReceived, 1, 'aReceived === 1');
	strictEqual(bReceived, 2, 'bReceived === 2');
});

test('Signal.connect and Signal.disconnect', function() {
	var signal = new mushroom.Signal();
	var handlerCalled = false;
	function handler() {
		handlerCalled = true;
	}
	signal.send();
	ok(!handlerCalled, 'handler not yet connected');
	signal.connect(handler);
	signal.send();
	ok(handlerCalled, 'handler connected');
	handlerCalled = false;
	signal.disconnect(handler);
	signal.send();
	ok(!handlerCalled, 'handler disconnected');
});

test('Signal.disconnectAll', function() {
	var signal = new mushroom.Signal();
	var handlerCalled = false;
	function handler() {
		handlerCalled = true;
	}
	signal.connect(handler);
	signal.disconnectAll();
	signal.send();
	ok(!handlerCalled, 'handler not called');
});

test('MessageQueue.push', function() {
	var queue = new mushroom.MessageQueue();
	var a = new mushroom.Notification({});
	strictEqual(queue.push(a), 0, 'queue.push(a) === 0');
	strictEqual(a.messageId, 0, 'a.messageId === 0');
	var b = new mushroom.Notification({});
	strictEqual(queue.push(b), 1, 'queue.push(a) === 1');
	strictEqual(b.messageId, 1, 'b.messageId === 1');
});

test('MessageQueue.push same message twice', function() {
	var queue = new mushroom.MessageQueue();
	var a = new mushroom.Notification({});
	queue.push(a);
	throws(function() {
		queue.push(a);
	}, mushroom.Exception, "message id already set");
});

test('MessageQueue.ack', function() {
	var queue = new mushroom.MessageQueue();
	queue.push(new mushroom.Notification({}));
	queue.push(new mushroom.Notification({}));
	queue.push(new mushroom.Notification({}));
	strictEqual(queue.messages.length, 3, 'queue.messages.length === 3');
	queue.ack(0);
	strictEqual(queue.messages.length, 2, 'queue.messages.length === 2');
	queue.ack(2);
	strictEqual(queue.messages.length, 0, 'queue.messages.length === 0');
});

test('MessageQueue.ack invalid message id', function() {
	var queue = new mushroom.MessageQueue();
	throws(function() {
		queue.ack(0);
	}, mushroom.Exception, "message id unknown to queue");
});

test('MessageQueue.forEach', function() {
	var queue = new mushroom.MessageQueue();
	queue.push(new mushroom.Notification({}));
	queue.push(new mushroom.Notification({}));
	var fCalled = 0;
	queue.forEach(function() {
		fCalled += 1;
	});
	strictEqual(fCalled, 2, 'fCalled === 2');
});
