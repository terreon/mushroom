===============
Webexec example
===============

The server executes an application (currently `ping localhost`) and
sends the standard output to all connected browsers.

Server - examples/webexec.py
----------------------------

.. literalinclude:: ../../examples/webexec.py


Client - examples/webexec.html
------------------------------

.. literalinclude:: ../../examples/webexec.html
    :language: html
