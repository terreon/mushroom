======================
Remote control example
======================

Read-eval-print loop which allows remote control of the connected
browsers. Start the server and type `help` to get a list of supported
commands.


Server - remote-control.py
--------------------------

.. literalinclude:: ../../examples/remote-control.py


Client - remote-control.html
----------------------------

.. literalinclude:: ../../examples/remote-control.html
    :language: html
