============
Chat example
============

Simple chat room example. The client code uses
Knockout_ and jQuery_.

.. _Knockout: http://knockoutjs.com/
.. _jQuery: http://jquery.com/

Server - examples/chat-server.py
--------------------------------

.. literalinclude:: ../../examples/chat-server.py


Client - examples/chat.html
---------------------------

.. literalinclude:: ../../examples/chat.html
    :language: html
