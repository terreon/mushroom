.. _examples:

========
Examples
========

Please see :ref:`install` how to set up mushroom and run the examples.

The examples can be found in the `examples` directory of the source tree.
The following examples are currently available:

.. toctree::
    :maxdepth: 1

    ping
    time-pusher
    chat
    remote-control
    webexec
    click-game
