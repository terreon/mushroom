==================
Click game example
==================

In this very basic game players must click an appearing square as
quick as possible in order to score points.

This example uses jQuery_.

.. _jQuery: http://jquery.com/

Server - examples/click-game.py
-------------------------------

.. literalinclude:: ../../examples/click-game.py


Client - examples/click-game.html
---------------------------------

.. literalinclude:: ../../examples/click-game.html
    :language: html
