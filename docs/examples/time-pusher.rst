===================
Time pusher example
===================

The server pushes the current time every second and the browser displays it.

Server - examples/time-pusher-server.py
---------------------------------------

.. literalinclude:: ../../examples/time-pusher-server.py


Client - examples/time-pusher.html
----------------------------------

.. literalinclude:: ../../examples/time-pusher.html
    :language: html
