.. _pyapi-django-support:

========================================
Django Support - mushroom.django_support
========================================

.. contents::
    :local:

.. currentmodule:: mushroom.django_support

.. automodule:: mushroom.django_support
    :members:
    :undoc-members:
