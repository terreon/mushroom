============================
Simple API - mushroom.simple
============================

.. contents::
    :local:

.. currentmodule:: mushroom.simple

.. automodule:: mushroom.simple
    :members:
    :undoc-members:
