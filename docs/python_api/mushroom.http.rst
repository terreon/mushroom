=============================================
HTTP utilities and transports - mushroom.http
=============================================

.. contents::
    :local:

.. currentmodule:: mushroom.http


Request and response
====================

.. autoclass:: HttpRequest
    :members:
    :undoc-members:

.. autoclass:: HttpResponse
    :members:
    :undoc-members:

.. autoclass:: JsonResponse
    :members:
    :undoc-members:


Exceptions
==========

.. autoclass:: HttpError
    :members:
    :undoc-members:

.. autoclass:: HttpUnauthorized
    :members:
    :undoc-members:

.. autoclass:: HttpNotFound
    :members:
    :undoc-members:

.. autoclass:: HttpMethodNotAllowed
    :members:
    :undoc-members:

.. autoclass:: HttpInternalServerError
    :members:
    :undoc-members:

.. autoclass:: HttpNotImplemented
    :members:
    :undoc-members:


Transports
==========

.. autoclass:: HttpTransport
    :members:
    :undoc-members:

.. autoclass:: PollTransport
    :members:
    :undoc-members:

.. autoclass:: WebSocketTransport
    :members:
    :undoc-members:
