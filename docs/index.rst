mushroom – real-time web messaging
==================================

.. include:: ../README

Table of contents
-----------------

.. toctree::
   :maxdepth: 2

   install.rst
   protocol.rst
   cors.rst
   examples/index
   python_api/index
   faq.rst
   license.rst
.. changelog.rst
.. features.rst
.. bugs.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

